export const protocol = 'http://';
export const host = '192.168.0.26';
export const port = '';
export const root = '/onboard/api';
export const empleadoSrv = '/employee';
export const appsOps = '/applications/';
export const identificarOps = '/identify/';
export const setPassOps = '/password';
export const proxy = '/onboardapi';
export const captcha = '/captcha';
export const clientId = 'onboard';
export const realm = 'indra';
export const auth = '/auth';
export const redirectUri = '/onboard/index.html';
export const piwikScriptUrl = '/piwik/piwik.js';
export const piwikTrackerUrl = '/piwik/piwik.php';
export const piwikSite = 11;

export const environment = {
  production: false,
  uriSrv: `${protocol}${host}${port}${root}${empleadoSrv}`,
  appsURI: `${protocol}${host}${port}${root}${empleadoSrv}${appsOps}`,
  identificarURI: `${protocol}${host}${port}${root}${empleadoSrv}${identificarOps}`,
  setpassURI: `${protocol}${host}${port}${root}${empleadoSrv}${setPassOps}`,
  issuer : `http://192.168.0.26:8080/auth/realms/indra`,
  captchaURL : `${protocol}${host}${port}${root}${captcha}`,
  redirectUri : `http://192.168.0.26:8100`,
  clientId : `onboard`,
  realm : `${realm}`,
  keycloakServerURL : `${protocol}${host}${port}${auth}`,
  tokenUrl: `${protocol}${host}${auth}/realms/${realm}/protocol/openid-connect/token`,
  piwikScriptUrl : `${protocol}${host}${piwikScriptUrl}`,
  piwikTrackerUrl : `${protocol}${host}${piwikTrackerUrl}`,
  piwikSite: piwikSite
};
