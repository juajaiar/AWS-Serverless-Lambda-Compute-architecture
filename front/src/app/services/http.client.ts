import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HttpClientImpl {

  constructor(private httpClient : HttpClient,private oauthService : OAuthService){}

  post(url:string, body: any): Observable<any>{

    const httpHeaders = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'POST',
      'Access-Control-Allow-Origin': '*',
      'Authorization' : "Bearer " + this.oauthService.getAccessToken()
    }
    
    return this.httpClient.post(url,body,{headers: httpHeaders});
  }
  
}