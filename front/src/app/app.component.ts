
import { HttpParams } from '@angular/common/http';
import { HttpClientImpl } from './services/http.client';
import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  
  constructor(private httpClientImpl : HttpClientImpl){}

  title = 'reserva-salas-front';

  clickMe(){

    const url : string = "http://localhost:8081/"
    const data = new HttpParams({
      fromObject: {
        'qaasdf': '1234',
        'asdfasfsdf': 'asdfsdf'
      }
    });

    this.httpClientImpl
    .post(url,data)
    .subscribe(response => console.log(response))
  }
}
